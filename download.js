var glob = require('glob'),
	fs = require('fs'),
	shelljs = require('shelljs/global'),
	request = require('request'),
	options = [ ],
	downloadsFolder = "/volume1/Downloads/In Progress/",
	completedFolder = "/volume1/Downloads/Completed/",
	key = '';

if (exec('ps | grep download.js | grep node | grep -v /bin/sh', { silent: true }).output.split(/\n/).length > 2) {
	console.log('put.io download script is already running');
	exit();
};

console.log('put.io download script');

console.log('checking for downloaded files...');

var url = 'https://put.io/v2/files/list?oauth_token=' + key;

request(
	{
	    url: url,
	    json: true
	},
	function (error, response, body) {
	    if (! error && response.statusCode === 200) {
	        for (var i = 0; i < body.files.length; i++) {
	        	var file = body.files[i];

	        	if (file.content_type == 'application/x-directory') {
	        		requestFolder(file);
	        	}
	        	else {
	        		requestDownload(file.name, file);
	        	}
	        }

	        if (body.files.length === 0) {
	        	console.log('no files found! cleaning up instead');

	        	cleanFolders(downloadsFolder);
				cleanFolders(completedFolder);

	        	console.log('peace out!');
	        }
	    }
    }
);

function cleanFolders(path) {
	var folders = glob.sync(path + '*/', options);

	for (var i = 0; i < folders.length; i++) {
	  	var folder = folders[i];

	  	if (! folder.match(/\.*@eaDir.*/)) {
	  		var files = glob.sync(folder + '*', options);

	  		if (files.length === 0) {
	  			console.log('deleting empty folder ' + folder + '...');

	  			try {
	  				fs.rmdirSync(folder);
	  			}
	  			catch(e) { }
	  		}
	  	}
	}
};

function createFolder(folder_name) {
	console.log('creating directory for ' + folder_name + '...');

	try {
		fs.mkdirSync(downloadsFolder + folder_name);
	}
	catch(e) { }

	try {
		fs.mkdirSync(completedFolder + folder_name);
		fs.chmodSync(completedFolder + folder_name, "777");
	}
	catch(e) { }
};

function requestFolder(file) {
	var url = 'https://put.io/v2/files/list?parent_id=' + file.id + '&oauth_token=' + key;

	request(
		{
		    url: url,
		    json: true
		},
		function (error, response, body) {
		    if (! error && response.statusCode === 200) {
		        for (var i = 0; i < body.files.length; i++) {
	       			requestDownload(file.name, body.files[i]);
	        	}
		    }
	    }
	);
};

function requestDownload(folder, file) {
	var url = 'https://put.io/v2/files/' + file.id + '/download?oauth_token=' + key,
		path = downloadsFolder + folder + '/' + file.name,
		completedPath = completedFolder + folder + '/' + file.name;

	createFolder(folder);

	console.log('requesting download for ' + file.name + '...');

	exec('wget -O "' + path + '" ' + url, { silent: true }, function() {
		console.log('deleting file from put.io ' + file.name + '...');
		exec("curl --data 'file_ids=" + file.id + "&oauth_token=" + key + "' 'https://put.io/v2/files/delete'", { silent: true} );

		createFolder(folder);

		console.log('moving file to completed folder and setting proper permissions ');
		fs.renameSync(path, completedPath);
		fs.chmodSync(completedPath, "777");
	});
};
