var glob = require('glob'),
		fs = require('fs'),
		shelljs = require('shelljs/global'),
		request = require('request'),
		options = [ ],
		key = '';

if (exec('ps | grep blackhole.js | grep node | grep -v /bin/sh', { silent: true }).output.split(/\n/).length > 2) {
	console.log('put.io blackhole script is already running');
	exit();
};

console.log('put.io blackhole script');

console.log('checking for torrent files...');
glob('/volume1/Downloads/Torrents/*.torrent', options, function (er, files) {
  if (! er) {
  	for (var i = 0; i < files.length; i++) {
  		var file = files[i];

  		console.log('found ' + file + '...');

  		console.log('uploading ' + file + '...');
		exec("curl -i -s -F file='@" + file + "' 'https://upload.put.io/v2/files/upload?oauth_token=" + key + "'", { silent: true });

  		console.log('deleting ' + file + '...');
  		fs.unlinkSync(file);
  	}
  }
});

console.log('clearing completed transfers...');
exec("curl -s -X POST 'https://put.io/v2/transfers/clean?oauth_token=" + key + "'", { silent: true });
request(
	{
	    url: 'https://put.io/v2/transfers/list?oauth_token=' + key,
	    json: true
	},
	function (error, response, body) {
	    if (! error && response.statusCode === 200) {
	        for (var i = 0; i < body.transfers.length; i++) {
	        	var transfer = body.transfers[i];

	        	if (transfer.finished_at && transfer.file_id) {
	        		console.log('clearing ' + transfer.name + '...');
	        		exec("curl -s --data 'transfer_ids=" + transfer.id + "' 'https://put.io/v2/transfers/cancel?oauth_token=" + key + "'", { silent: true });
	        	}
	        }
	    }
    }
);

console.log('deleting empty folders...');
request(
	{
	    url: 'https://put.io/v2/files/list?oauth_token=' + key,
	    json: true
	},
	function (error, response, body) {
	    if (! error && response.statusCode === 200) {
	        for (var i = 0; i < body.files.length; i++) {
	        	var file = body.files[i];

	        	if (file.content_type == 'application/x-directory' && file.size === 0) {
	        		console.log('deleting folder ' + file.name + '...');
	        		exec("curl -s --data 'file_ids=" + file.id + "&oauth_token=" + key + "' 'https://put.io/v2/files/delete'", { silent: true });
	        	}
	        }
	    }
    }
);

function getDateTime() {
    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;
};
